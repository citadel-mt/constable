import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.jetbrains.kotlin.jvm").version("1.3.21")
    java
    application
}

tasks.withType<KotlinCompile>().all {
    kotlinOptions.freeCompilerArgs += "-Xuse-experimental=kotlin.Experimental"
    kotlinOptions.jvmTarget = "1.8"
}

val fxModules = arrayListOf("base", "graphics", "controls")

tasks.withType<JavaCompile>().all {
    doFirst {
        options.compilerArgs = mutableListOf(
            "--module-path", classpath.asPath,
            "--add-modules", fxModules.joinToString(separator = ",") { "javafx.$it" })
    }
}

application {
    tasks.withType<JavaExec>().all {
        doFirst {
            jvmArgs = mutableListOf(
                "--module-path", classpath.asPath,
                "--add-modules", fxModules.joinToString(separator = ",") { "javafx.$it" })
        }
    }
    mainClassName = "org.mt.ui.Main"
}

val platform = with(org.gradle.internal.os.OperatingSystem.current()) {
    when {
        isWindows -> "win"
        isLinux -> "linux"
        isMacOsX -> "mac"
        else -> "unknown"
    }
}

repositories {
    mavenCentral()
    jcenter()
    maven("https://kotlin.bintray.com/ktor")
    maven("https://jitpack.io")
}

val ktor_version = "1.1.3"
val tornadofx_version = "1.7.17"
val junit_version = "5.1.0"

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    // https://mvnrepository.com/artifact/org.jetbrains.kotlinx/kotlinx-coroutines-core
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.2.1")
    //ktor-client
    implementation("io.ktor:ktor-client-core:$ktor_version")
    implementation("io.ktor:ktor-client-core-jvm:$ktor_version")
    implementation("io.ktor:ktor-client-cio:$ktor_version")
    implementation("com.github.salomonbrys.kotson:kotson:2.5.0")
    //tornadofx
    implementation("no.tornado:tornadofx:$tornadofx_version")
    for (module in fxModules) {
        implementation("org.openjfx:javafx-$module:11:$platform")
    }
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-javafx:1.2.1")
    //notifications
    //web-dav client
    implementation("com.github.lookfirst:sardine:5.8")
    implementation("com.sun.xml.bind:jaxb-core:2.3.0.1")
    implementation("com.sun.xml.bind:jaxb-impl:2.3.2")
    implementation("javax.xml.bind:jaxb-api:2.3.1")

    testImplementation("org.junit.jupiter:junit-jupiter-api:$junit_version")
    testRuntime("org.junit.jupiter:junit-jupiter-engine:$junit_version")
}

java {
    tasks.withType<Jar>().all {
        manifest {
            attributes ("Main-Class" to "org.mt.ui.Main")
        }
        configurations["compileClasspath"].forEach { file: File ->
            from(zipTree(file.absoluteFile))
        }
    }
}