package org.mt.ui

import javafx.beans.property.SimpleBooleanProperty
import javafx.scene.layout.StackPane
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.mt.Constants
import org.mt.SavesWorker
import org.mt.logic.Server
import org.mt.logic.ServerConnector
import org.mt.logic.Status
import org.mt.logic.getServerType
import org.mt.ui.views.FilesView
import tornadofx.*
import java.io.FileWriter
import java.nio.file.Files
import java.nio.file.NoSuchFileException
import java.nio.file.Paths
import java.util.*
import javax.json.*

object Controller {

    val indicatorVisibility = SimpleBooleanProperty(false)
    private val serverItems = arrayListOf<Server>()
    private val connector = ServerConnector()

    private val ioScope = CoroutineScope(Dispatchers.IO)
    private val mainScope = CoroutineScope(Dispatchers.Main)

    fun checkAvailableServers(callback: (server: Server) -> Unit) {
        checkServersFromSaves(callback)
        loadServersInView(callback)
    }

    fun checkServersFromSaves(callback: (server: Server) -> Unit) {
        val jsReader = try {
            Json.createReader(Files.newInputStream(Paths.get("${Constants.pathToSaves}/servers.json")))
        } catch (exc: Exception) {
            Files.createFile(Paths.get("${Constants.pathToSaves}/servers.json"))
            Files.writeString(Paths.get("${Constants.pathToSaves}/servers.json"), "[]")
            Json.createReader(Files.newInputStream(Paths.get("${Constants.pathToSaves}/servers.json")))
            // Json.createReader(FileInputStream(File("${Constants.pathToSaves}/servers.json")))
        }
        val jsArray = jsReader.readArray()
        for (serv in jsArray) {
            ioScope.launch {
                val server = connector.checkServer(serv.asJsonObject().getString("host"))
                mainScope.launch {
                    if (server != null) {
                        server.status = Status.CONNECTED
                        server.directory = serv.asJsonObject().getString("directory")
                        serverItems.add(server)
                        callback(server)
                    } else {
                        val prevServer = Server(
                            host = serv.asJsonObject().getString("host"),
                            name = serv.asJsonObject().getString("name"),
                            type = serv.asJsonObject().getString("type").getServerType(),
                            version = serv.asJsonObject().getString("version"),
                            status = Status.INACCESSIBLE,
                            directory = serv.asJsonObject().getString("directory")
                        )
                        serverItems.add(prevServer)
                        callback(prevServer)
                    }
                }
            }
        }
    }

    fun loadServersInView(callback: (server: Server) -> Unit) {
        indicatorVisibility.set(true)
        ioScope.launch {
            connector.loadServersList {server ->
                if (serverItems.find { it.name == server.name} == null) {
                    mainScope.launch {
                        serverItems.add(server)
                        callback(server)
                    }
                }
            }
        }.invokeOnCompletion {
            indicatorVisibility.set(false)
        }
    }

    fun openServerOptions(stackPane: StackPane, server: Server) {
        if (server.status == Status.CONNECTED) {
            stackPane.children.add(FilesView(stackPane, server).root)
        }
    }

    fun setDirectory(stackPane: StackPane, server: Server) {
        val dir = chooseDirectory()
        if (dir != null) {
            server.directory = dir.absolutePath
            val props = Properties()
            val currentDate = Date()
            props["date_created"] = currentDate.toString()
            props["date_updated"] = currentDate.toString()
            props.store(FileWriter(dir.absolutePath+"/.citadel"), "Citadel dir config file")
            SavesWorker.updateServer(server)
            stackPane.children.removeAt(stackPane.children.size-1)
            stackPane.children.add(FilesView(stackPane, server).root)
        }
    }

    fun resetDirectory(stackPane: StackPane, server: Server) {
        Files.delete(Paths.get(server.directory+"/.citadel"))
        server.directory = "-"
        SavesWorker.updateServer(server)
        stackPane.children.removeAt(stackPane.children.size-1)
        stackPane.children.add(FilesView(stackPane, server).root)
    }
}