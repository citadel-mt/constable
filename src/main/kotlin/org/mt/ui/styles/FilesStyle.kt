package org.mt.ui.styles

import javafx.scene.paint.Color
import tornadofx.*

class FilesStyle: Stylesheet() {

    companion object {
        val filesViewRoot by cssclass()
    }

    init {
        filesViewRoot {
            prefWidth = MainStyle.centerWidth.px
            prefHeight = MainStyle.windowHeight.px
            backgroundColor += Color.WHITESMOKE
        }
    }
}