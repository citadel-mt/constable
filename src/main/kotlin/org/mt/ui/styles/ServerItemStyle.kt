package org.mt.ui.styles

import javafx.geometry.Pos
import javafx.scene.layout.BackgroundPosition
import javafx.scene.layout.BackgroundRepeat
import javafx.scene.layout.BackgroundSize
import javafx.scene.paint.Color
import org.mt.Constants
import tornadofx.*
import java.net.URI

class ServerItemStyle: Stylesheet() {

    companion object {
        val serverAvailableColor = Color.web("#888")!!
        val serverConnectedColor = Color.web("#00FF00")!!
        val serverInaccessibleColor = Color.web("#FF0000")!!

        val serverItemViewRoot by cssclass()
        val buttonNonStarred by cssclass()
        val buttonStarred by cssclass()
        val buttonInfo by cssclass()
    }

    init {
        serverItemViewRoot {
            prefHeight = 40.px

            button {
                val size = 30
                alignment = Pos.TOP_CENTER
                prefWidth = size.px; prefHeight = size.px
                backgroundColor += Color.TRANSPARENT
                backgroundRepeat += BackgroundRepeat.NO_REPEAT to BackgroundRepeat.NO_REPEAT
                backgroundPosition += BackgroundPosition.CENTER
                backgroundSize += BackgroundSize(size.toDouble() - 4.0, size.toDouble() - 4.0,
                        false, false, false, false)
            }
            buttonNonStarred {
                backgroundImage += URI.create("file:${Constants.pathToDrawable}/non-starred.png")
            }
            buttonStarred {
                backgroundImage += URI.create("file:${Constants.pathToDrawable}/starred.png")
            }
            buttonInfo {
                backgroundImage += URI.create("file:${Constants.pathToDrawable}/dialog-question.png")
            }
        }
    }
}