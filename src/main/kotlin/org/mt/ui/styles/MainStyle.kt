package org.mt.ui.styles

import javafx.geometry.Pos
import javafx.scene.layout.BackgroundPosition
import javafx.scene.layout.BackgroundRepeat
import javafx.scene.layout.BackgroundSize
import javafx.scene.paint.Color
import org.mt.Constants
import tornadofx.*
import java.net.URI

class MainStyle: Stylesheet() {

    companion object {
        const val windowWidth = 700
        const val windowHeight = 450
        const val centerWidth = 300

        val mainViewRoot by cssclass()
        val leftPane by cssclass()
        val rightPane by cssclass()
        val serverList by cssclass()

        val backButton by cssclass()
        val refreshButton by cssclass()
    }

    init {
        mainViewRoot {
            prefWidth = windowWidth.px; prefHeight = windowHeight.px
            toolBar {
                prefHeight = 35.px
                button {
                    val size = 30
                    alignment = Pos.TOP_CENTER
                    prefWidth = size.px; prefHeight = size.px
                    backgroundColor += Color.TRANSPARENT
                    backgroundRepeat += BackgroundRepeat.NO_REPEAT to BackgroundRepeat.NO_REPEAT
                    backgroundPosition += BackgroundPosition.CENTER
                    backgroundSize += BackgroundSize(size.toDouble() - 4.0, size.toDouble() - 4.0,
                            false, false, false, false)
                }
                backButton {
                    backgroundImage += URI.create("file:${Constants.pathToDrawable}/edit-redo.png")
                }
                refreshButton {
                    backgroundImage += URI.create("file:${Constants.pathToDrawable}/view-refresh.png")
                }
            }
            leftPane {
                prefWidth = 100.px
            }
            rightPane {
                prefWidth = 300.px
            }
            serverList {
                prefWidth = centerWidth.px; prefHeight = windowHeight.px
            }
        }
    }
}