package org.mt.ui.views

import javafx.beans.property.SimpleObjectProperty
import javafx.beans.value.ObservableValue
import javafx.scene.control.Label
import javafx.scene.layout.Pane
import javafx.scene.layout.StackPane
import org.mt.SavesWorker
import org.mt.logic.Server
import org.mt.logic.Status
import org.mt.ui.Controller
import org.mt.ui.styles.ServerItemStyle
import tornadofx.*

class ServerItemView(private val stackPane: StackPane,
                     val server: Server) : View("Server Item") {

    init {
        importStylesheet<ServerItemStyle>()
    }

    private val statusProperty = SimpleObjectProperty<Status>(server.status)

    override val root = anchorpane {
        setOnMouseClicked {
            Controller.openServerOptions(stackPane, server)
        }
        addClass(ServerItemStyle.serverItemViewRoot)
        hbox {
            vbox {
                label(server.name)
                statusLabel(statusProperty)
            }
            button {
                if (statusProperty.value == Status.AVAILABLE) {
                    addClass(ServerItemStyle.buttonNonStarred)
                } else {
                    addClass(ServerItemStyle.buttonStarred)
                }
                setOnMouseClicked {
                    if (statusProperty.value == Status.AVAILABLE) {
                        removeClass(ServerItemStyle.buttonNonStarred)
                        addClass(ServerItemStyle.buttonStarred)
                        statusProperty.set(Status.CONNECTED)
                        SavesWorker.saveServer(server)
                    } else if (statusProperty.value == Status.CONNECTED) {
                        removeClass(ServerItemStyle.buttonStarred)
                        addClass(ServerItemStyle.buttonNonStarred)
                        statusProperty.set(Status.AVAILABLE)
                        SavesWorker.deleteServer(server)
                    }
                }
            }
            button {
                addClass(ServerItemStyle.buttonInfo)
            }
        }
    }

    private fun Pane.statusLabel(statusProp: SimpleObjectProperty<Status>, op: Label.() -> Unit = {}) {
        val label = Label(statusProp.value.toString())
        fun Status.getColor() = when (this) {
            Status.AVAILABLE -> ServerItemStyle.serverAvailableColor
            Status.CONNECTED -> ServerItemStyle.serverConnectedColor
            Status.INACCESSIBLE -> ServerItemStyle.serverInaccessibleColor
        }
        label.textFill = statusProp.value.getColor()
        statusProp.addListener { _: ObservableValue<out Status>, _: Status, newVal: Status ->
            label.text = newVal.toString()
            label.textFill = newVal.getColor()
        }
        label.op()
        this.children.add(label)
    }
}
