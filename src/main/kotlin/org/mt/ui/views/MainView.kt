package org.mt.ui.views

import javafx.collections.FXCollections
import javafx.geometry.Orientation
import javafx.scene.layout.AnchorPane
import javafx.scene.layout.Priority
import javafx.scene.layout.StackPane
import org.mt.logic.Server
import org.mt.logic.ServerType
import org.mt.logic.Status
import org.mt.ui.Controller
import org.mt.ui.styles.MainStyle
import tornadofx.*

class MainView: View("Main View") {

    init {
        importStylesheet<MainStyle>()
    }

    private val listItems = FXCollections.observableArrayList<AnchorPane>()

    lateinit var stackPane: StackPane

    override val root = borderpane {
        addClass(MainStyle.mainViewRoot)
        top = toolbar {
            anchorpane {
                hgrow = Priority.ALWAYS
                button {
                    addClass(MainStyle.backButton)
                    anchorpaneConstraints {
                        leftAnchor = 0.0
                    }
                    setOnMouseClicked {
                        if (stackPane.children.size > 1) {
                            stackPane.children.removeAt(stackPane.children.size - 1)
                        }
                    }
                }
                button {
                    addClass(MainStyle.refreshButton)
                    anchorpaneConstraints {
                        rightAnchor = 0.0
                    }
                    setOnMouseClicked {
                        listItems.clear()
                        Controller.checkAvailableServers {
                            insertServerItem(it)
                        }
                    }
                }
            }
        }
        center = anchorpane {
            Controller.checkAvailableServers {
                insertServerItem(it)
            }
            hbox {
                flowpane {
                    addClass(MainStyle.leftPane)
                }
                separator(orientation = Orientation.VERTICAL)
                stackPane = stackpane {
                    anchorpane {
                        listview<AnchorPane> {
                            addClass(MainStyle.serverList)
                            items = listItems
                        }
                        progressindicator {
                            visibleWhen(Controller.indicatorVisibility)
                            anchorpaneConstraints {
                                topAnchor = 20.0
                                leftAnchor = 0.0
                                rightAnchor = 0.0
                            }
                        }
                    }
                }
                separator(orientation = Orientation.VERTICAL)
                flowpane {
                    addClass(MainStyle.rightPane)
                }
            }
        }
        //Test
        /*insertServerItem(Server(host="192.168.0.103:1234",
                name="samsung GT-7256",
                type=ServerType.SENESCHAL,
                version="1.0.0",
                status = Status.AVAILABLE))*/
    }

    fun insertServerItem(server: Server) {
        listItems.add(ServerItemView(stackPane, server).root)
    }
}