package org.mt.ui.views

import javafx.scene.layout.StackPane
import org.mt.logic.Server
import org.mt.ui.Controller
import org.mt.ui.styles.FilesStyle
import tornadofx.*

class FilesView(private val stackPane: StackPane,
                private val server: Server): View("Files") {

    init {
        importStylesheet<FilesStyle>()
    }

    override val root = anchorpane {
        addClass(FilesStyle.filesViewRoot)
        if (server.directory == "-") {
            vbox {
                label("Directory is not set")
                button("Choose directory") {
                    setOnMouseClicked {
                        Controller.setDirectory(stackPane, server)
                    }
                }
            }
        } else {
            vbox {
                label("Synchronization")
                hbox {
                    button("Download")
                    button("Upload")
                }
                button("Reset directory") {
                    setOnMouseClicked {
                        Controller.resetDirectory(stackPane, server)
                    }
                }
            }
        }
    }
}