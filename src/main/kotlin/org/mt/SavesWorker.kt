package org.mt

import org.mt.logic.Server
import tornadofx.*
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import javax.json.Json

object SavesWorker {

    fun saveServer(server: Server) {
        val jsReader = Json.createReader(
                FileInputStream(File("${Constants.pathToSaves}/servers.json")))
        val jsArray = jsReader.readArray()
        val jsServer = JsonBuilder()
                .add("host", server.host)
                .add("name", server.name)
                .add("type", server.type.toString())
                .add("version", server.version)
                .add("directory", server.directory)
                .build()
        println(jsServer)
        val newJsArray = Json.createArrayBuilder(jsArray)
                .add(jsServer)
                .build()
        val jsWriter = Json.createWriter(
                FileOutputStream(File("${Constants.pathToSaves}/servers.json")))
        jsWriter.writeArray(newJsArray)
    }

    fun updateServer(server: Server) {
        val jsReader = Json.createReader(
                FileInputStream(File("${Constants.pathToSaves}/servers.json")))
        val jsArray = jsReader.readArray()
        val newJsArray = Json.createArrayBuilder()
        for (serv in jsArray) {
            if (server.name != serv.asJsonObject().getString("name")) {
                newJsArray.add(serv)
            } else {
                val jsServer = JsonBuilder()
                        .add("host", server.host)
                        .add("name", server.name)
                        .add("type", server.type.toString())
                        .add("version", server.version)
                        .add("directory", server.directory)
                        .build()
                println(jsServer)
                newJsArray.add(jsServer)
            }
        }
        val jsWriter = Json.createWriter(
                FileOutputStream(File("${Constants.pathToSaves}/servers.json")))
        jsWriter.writeArray(newJsArray.build())
    }

    fun deleteServer(server: Server) {
        val jsReader = Json.createReader(
                FileInputStream(File("${Constants.pathToSaves}/servers.json")))
        val jsArray = jsReader.readArray()
        val newJsArray = Json.createArrayBuilder()
        for (serv in jsArray) {
            if (server.name != serv.asJsonObject().getString("name")) {
                newJsArray.add(serv)
            }
        }
        val jsWriter = Json.createWriter(
                FileOutputStream(File("${Constants.pathToSaves}/servers.json")))
        jsWriter.writeArray(newJsArray.build())
    }
}