package org.mt.logic

enum class ServerType {
    SENESCHAL, UNKNOWN;

    override fun toString() =
        when (this) {
            SENESCHAL -> "Seneschal"
            UNKNOWN -> "Unknown"
        }
}

enum class Status {
    AVAILABLE, CONNECTED, INACCESSIBLE;

    override fun toString() =
        when (this) {
            AVAILABLE -> "available"
            CONNECTED -> "connected"
            INACCESSIBLE -> "inaccessible"
        }
}

fun String.getServerType(): ServerType =
        when(this) {
            "seneschal" -> ServerType.SENESCHAL
            else -> ServerType.UNKNOWN
        }

data class Server(val host: String,
                  val name: String,
                  val type: ServerType,
                  val version: String,
                  var status: Status,
                  var directory: String = "-")