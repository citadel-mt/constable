package org.mt.logic

import com.github.salomonbrys.kotson.fromJson
import com.github.salomonbrys.kotson.registerTypeAdapter
import com.google.gson.GsonBuilder
import io.ktor.client.HttpClient
import io.ktor.client.engine.cio.CIO
import io.ktor.client.request.delete
import io.ktor.client.request.get
import io.ktor.client.request.header
import io.ktor.client.request.post
import io.ktor.content.ByteArrayContent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.io.readUTF8Line
import org.mt.Constants

@UseExperimental(io.ktor.util.KtorExperimentalAPI::class)
class FilesClient(private val server: Server) {

    private val client = HttpClient(CIO)
    private val gson = GsonBuilder()
            .registerTypeAdapter(serverErrorDeserializer)
            .create()
    private val ioScope = CoroutineScope(Dispatchers.IO)

    data class DeleteFileResult(val data: String)

    suspend fun deleteFile(path: String): DeleteFileResult {
        try {
            val result = client.delete<String>("http://${server.host}:${Constants.defaultPort}/files/$path")
            return gson.fromJson(result)
        } catch (err: io.ktor.client.features.BadResponseStatusException) {
            throw gson.fromJson<ServerError>(err.response.content.readUTF8Line()!!)
        }
    }

    data class PostFileResult(val data: String)

    suspend fun postFile(filePath: String, file: ByteArray): PostFileResult {
        try {
            val result = client.post<String>("http://${server.host}:${Constants.defaultPort}/files/") {
                header("filename", filePath)
                body = ByteArrayContent(file)
            }
            return gson.fromJson(result)
        } catch (err: io.ktor.client.features.BadResponseStatusException) {
            throw gson.fromJson<ServerError>(err.response.content.readUTF8Line()!!)
        }
    }

    suspend fun getFile(path: String): ByteArray {
        try {
            val result = client.get<ByteArray>("http://${server.host}:${Constants.defaultPort}/files/${path.replace("/", "%2F")}")
            return result
        } catch (err: io.ktor.client.features.BadResponseStatusException) {
            throw gson.fromJson<ServerError>(err.response.content.readUTF8Line()!!)
        }
    }

    data class CreateDirResult(val data: String)

    suspend fun createDir(dir: String): CreateDirResult {
        try {
            val result = client.post<String>("http://${server.host}:${Constants.defaultPort}/files/dir/${dir.replace("/", "%2F")}")
            return gson.fromJson(result)
        } catch (err: io.ktor.client.features.BadResponseStatusException) {
            throw gson.fromJson<ServerError>(err.response.content.readUTF8Line()!!)
        }
    }

    fun disconnect() {
        client.close()
    }

    protected fun finalize() {
        client.close()
    }
}