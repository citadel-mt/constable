package org.mt.logic

import com.github.sardine.SardineFactory

class WebDavClient {

    private val sardine = SardineFactory.begin()

    public fun upload() {

    }

    public fun download() {

    }

    protected fun finalize() {
        sardine.shutdown()
    }
}