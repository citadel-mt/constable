package org.mt.logic

import com.github.salomonbrys.kotson.get
import com.github.salomonbrys.kotson.jsonDeserializer
import kotlinx.coroutines.*
import io.ktor.client.HttpClient
import io.ktor.client.engine.cio.CIO
import io.ktor.client.request.get
import org.mt.Constants
import java.net.InetAddress
import kotlin.Exception

const val identifier = "Citadel"

data class ServerError(val error_code: Int, val error_message: String): Exception()

val serverErrorDeserializer = jsonDeserializer {
    ServerError(it.json["error_code"].asInt,
                it.json["error_message"].asString)
}

@UseExperimental(io.ktor.util.KtorExperimentalAPI::class)
class ServerConnector(var pingTimeout: Int = 1000) {

    private val client = HttpClient(CIO)

    private suspend fun checkHost(host: String) =
        withContext(Dispatchers.IO) {
            InetAddress.getByName(host).isReachable(pingTimeout)
        }

    suspend fun checkServer(host: String): Server? {
        try {
            val text = client.get<String>("http://$host:$Constants.defaultPort/info")
            val (className, params) = text.split("(")
            if (className == identifier) {
                val (server, name, version) = params.trimEnd(')').split(",")
                val serverItem = Server(
                        host = host,
                        name = name.split("=").component2(),
                        type = server.split("=").component2().getServerType(),
                        version = version.split("=").component2(),
                        status = Status.AVAILABLE
                )
                return serverItem
            }
            return null
        } catch (exc: Exception) {
            println("Host $host is not a citadel-server")
            return null
        }
    }

    suspend fun loadServersList(callback: (server: Server) -> Unit)
            = withContext(Dispatchers.Default) {
        val time = System.currentTimeMillis()
        val reachableHosts = arrayListOf<String>()
        launch {
            for (i in 1..254) {
                launch {
                    val host = "$Constants.subnet.$i"
                    if (checkHost(host)) {
                        reachableHosts.add(host)
                    }
                }
            }
        }.join()
        println(reachableHosts)
        val availableServers = arrayListOf<Server>()
        launch {
            for (host in reachableHosts) {
                launch {
                    val server = checkServer(host)
                    if (server != null) {
                        availableServers.add(server)
                        callback(server)
                    }
                }
            }
        }.join()
        println(availableServers)
        println("totalTime: ${System.currentTimeMillis() - time}")
        availableServers
    }

    protected fun finalize() {
        client.close()
    }
}