package org.mt

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.mt.logic.FilesClient
import org.mt.logic.Server
import org.mt.logic.ServerError
import org.mt.logic.ServerType
import org.mt.logic.Status
import java.nio.file.Files
import java.nio.file.Paths

class FilesTest {

    lateinit var filesClient: FilesClient
    val ioScope = CoroutineScope(Dispatchers.IO)

    @BeforeEach
    fun createClient() {
        filesClient = FilesClient(Server(
                "192.168.0.119",
                "asusX751LN",
                ServerType.SENESCHAL,
                "1.0.0",
                Status.CONNECTED,
                "home/andrew/Документы/Java-projects/Constable/src/test/resources/clone"))
    }

    @AfterEach
    fun deleteClient() {
        filesClient.disconnect()
    }

    @Test
    @DisplayName("should get file from server")
    fun testGet() = runBlocking {
        ioScope.launch {
            withContext(Dispatchers.IO) {
                try {
                    val result = filesClient.getFile("testd2/test21.txt")
                    Files.write(Paths.get("${Constants.pathToTestDir}/test21.txt"), result)
                } catch (err: ServerError) {
                    println(err)
                }
            }
        }.join()
    }

    @Test
    @DisplayName("should create directory on server")
    fun testCreateDir() = runBlocking {
        ioScope.launch {
            withContext(Dispatchers.IO) {
                try {
                    val result = filesClient.createDir("testd3/testd31")
                    println(result)
                } catch (err: ServerError) {
                    println(err)
                }
            }
        }.join()
    }

    @Test
    @DisplayName("should post new file on a server")
    fun testPost() = runBlocking {
        ioScope.launch {
            withContext(Dispatchers.IO) {
                try {
                    val result = filesClient.postFile("testd2/client/clientFile.txt",
                            Files.readAllBytes(Paths.get("${Constants.pathToTestDir}/clientFile.txt")))
                    println(result)
                } catch (err: ServerError) {
                    println(err)
                }
            }
        }.join()
    }

    @Test
    @DisplayName("should delete file on a server")
    fun testDelete() = runBlocking {
        ioScope.launch {
            val result = filesClient.deleteFile("testd/test2.txt".replace("/", "%2F"))
            assertEquals(FilesClient.DeleteFileResult("testd/test2.txt"), result)
        }.join()
    }

    @Test
    @DisplayName("should return error if file is not exist")
    fun testDeleteNotExist() = runBlocking {
        ioScope.launch {
            try {
                filesClient.deleteFile("testd/test.txt".replace("/", "%2F"))
            } catch (err: ServerError) {
                assertEquals(err.error_code, 400)
                assertEquals(err.error_message, "No such file exception: testd/test.txt")
            }
        }.join()
    }
}