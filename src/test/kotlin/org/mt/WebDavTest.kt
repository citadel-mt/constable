package org.mt

import com.github.sardine.DavResource
import com.github.sardine.SardineFactory
import kotlinx.coroutines.*
import kotlinx.coroutines.io.jvm.javaio.toByteReadChannel
import kotlinx.coroutines.io.readRemaining
import kotlinx.io.core.readBytes
import java.io.*
import java.nio.file.Files
import java.nio.file.Paths

class WebDavTest {

    val sardine = SardineFactory.begin()

    /*@Test
    @kotlinx.io.core.ExperimentalIoApi
    fun testFileTest() = runBlocking {
        val resources  = sardine.list("http://192.168.0.103:1234/files/", 0)
        *//*for (res in resources) {
            println(res.path)
        }*//*
        //CoroutineScope(Dispatchers.IO).launch {
        cloneFiles("${Constants.pathToTestDir}/", resources[0])
        //}.join()
    }*/

    @kotlinx.io.core.ExperimentalIoApi
    suspend fun saveFile(remotePath: String, pathToSave: String) =
        withContext(Dispatchers.IO) {
            val `is` = sardine.get("http://192.168.0.103:1234${remotePath.replace(" ", "%20")}")
            `is`.toByteReadChannel().readRemaining().use {
                Files.write(Paths.get(pathToSave), it.readBytes())
            }
            Unit
        }

    @kotlinx.io.core.ExperimentalIoApi
    suspend fun cloneFiles(path: String, resource: DavResource): Unit = withContext(Dispatchers.IO) {
        val shortPath = resource.path.removePrefix("/files/").removeSuffix("/")
        println(shortPath)
        if (resource.isDirectory) {
            if (shortPath.isNotEmpty()) {
                Files.createDirectory(Paths.get(path + shortPath))
            }
            val dirResources = sardine.list("http://192.168.0.103:1234${resource.path.replace(" ", "%20")}")
            for (res in dirResources) {
                if (res.path != resource.path) {
                    cloneFiles(path, res)
                }
            }
        } else {
            saveFile(resource.path, path + shortPath)
        }
    }

    fun showFiles(resource: DavResource) {
        if (resource.isDirectory) {
            val dir_resources = sardine.list("http://192.168.0.103:1234${resource.path}")
            for (res in dir_resources) {
                if (res.path != resource.path) {
                    showFiles(res)
                }
            }
        } else {
            println(resource.path)
        }
    }
}